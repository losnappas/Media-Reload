"use strict"
const ACTIVE_TITLE = "Deactivate Reload for current tab";
const DBLCLICK_TITLE = "Click again to reload all media elements";
const INACTIVE_TITLE = "Activate Reload for current tab";
const ICON_ON = "icons/icon-on.svg";
const ICON_OFF = "icons/icon-off.svg";
const ICON_DBL = "icons/icon-dbl.svg";

var useWay = null;

function onError(error)
  {
    console.error(`Media Reload Error: ${error}`);
  }

async function updateBrowserAction(tabId) {
	tabId = tabId == null ? undefined : tabId;
	let title = await browser.browserAction.getTitle({tabId});
	let newTitle;
	let newIcon;
	switch (title){
		case DBLCLICK_TITLE:
			newTitle = ACTIVE_TITLE;
			newIcon = ICON_ON;
			break;
		case INACTIVE_TITLE:
			newTitle = DBLCLICK_TITLE;
			newIcon = ICON_DBL;
			break;
		case ACTIVE_TITLE:
		default:
			newTitle = INACTIVE_TITLE;
			newIcon = ICON_OFF;
	}

	  browser.browserAction.setIcon({
		path: newIcon,
		tabId: tabId
	  });
	  browser.browserAction.setTitle({
		title: newTitle,
		tabId: tabId
	  });

	return newTitle;
}

async function inject  (e){
	let title = await browser.browserAction.getTitle({tabId: e.id});
	let tabId = e.id;
	if (title === DBLCLICK_TITLE) {
		browser.tabs.executeScript({
			file: "/content_scripts/reloadAll.js",
			allFrames: true
		});
		return;
	}

	title = await updateBrowserAction(tabId);
	if (title === DBLCLICK_TITLE) {
		setTimeout(deactivate, 3000, tabId);
	}

	if (useWay === 'click'){
		browser.tabs.executeScript({
			file: "/content_scripts/reloadClick.js",
			allFrames: true
		});
	}
}

function deactivate(tabId) {
	updateBrowserAction(tabId);
}

// contextMenu method does this
function doThings(info, tab){
	// it doesn't matter how many times the script is injected into the page. The script doesn't leave anything behind after executing.
  	browser.tabs.executeScript({
		file: "/content_scripts/reloadContextMenu.js",
		frameId: info.frameId
	})
	.then( () => browser.tabs.sendMessage(tab.id, {url: info.srcUrl, mediaType: info.mediaType}) )
	.catch( (err)=> console.error("reloadListener error:", err)	);	
	
}


function reloadUseWay (result)  {
	useWay = result.reloadUseWay || 'context';	

	//if it doesn't exist or already exists then it pops an error message but who cares rly.
	if(useWay === 'context')
	{	
		let contexts = result.contexts || ["audio", "video", "image"];
		browser.contextMenus.remove("reload").then(() => {
			browser.contextMenus.create({
				id: "reload",
				title: "Reload",
				contexts: contexts,
				onclick: doThings
			});
		}).catch(e => console.log("Media reload: 'reloadUseWay' error.",e));
		reload=true;
	}
	else
	{
		//trying to remove a non-existing context menu item is ok
		browser.contextMenus.remove("reload");
	}
}

function afterReloadRepaint  (request, sender) {
	if(request.message && request.message === 'reloadClicked'){
		updateBrowserAction(sender.tab.id);
	}
}


function optionsChanged (request, sender, sendResponse)  {
	if(request === 'first' || (request.command != null && request.command === 'optionsChanged')){
		var gettingUseWay = browser.storage.local.get();
		gettingUseWay.then(reloadUseWay, onError);
	}

}

optionsChanged('first', null, null);

if(!browser.runtime.onMessage.hasListener(optionsChanged))
	browser.runtime.onMessage.addListener(optionsChanged);

//listener for browser action reloadClick method -message
if(!browser.runtime.onMessage.hasListener(afterReloadRepaint))
	browser.runtime.onMessage.addListener(afterReloadRepaint);


browser.browserAction.onClicked.addListener(inject);

