browser.storage.local.get('contexts').then(({contexts}) => {
	if (contexts==null) {
		contexts=["audio","img","video"];
	}

	let elements = document.querySelectorAll(contexts);
	Array.prototype.forEach.call(elements, element => {
		let src = element.src;
		element.src = '';
		element.src = src;
	})
})

