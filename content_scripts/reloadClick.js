'use strict'

function apply(){
	//console.log('in apply')
	if (window.reloadInjected) {
		//console.log('2nd click.')
		// Second click.
		window.reloadInjected = false
		//Remove the listener since this was 2nd click.
		window.removeEventListener('click', window.reload, true);

		return
	}
	//console.log('1st click.')
	// First click.
	window.reloadInjected = true

	//console.log('in apply2')
	// removeEventListener needs this to be 'window.reload'.
	// Otherwise it doesn't work.
	window.reload = e => {
		//console.log('reloading!:', e);
		let tag = e.target.tagName.toLowerCase() ;
		e.preventDefault();

		if (['audio','img','video'].indexOf(tag) !== -1) {
			e.stopPropagation();
		}

		browser.storage.local.get('contexts').then(({contexts}) => {
			if (contexts==null) {
				contexts=["audio","img","video"];
			}
			let imgidx = contexts.indexOf('image');
			if (imgidx !== -1) {
				contexts[imgidx]="img";
			}
			if (contexts.indexOf(tag) !== -1)
			{
				// Reset the clicked media's source.
				// Effectively reloading it.
				//console.log("resetting via click!");
				let src = e.target.currentSrc;
				e.target.src='';
				e.target.src=src;
			}

		}).catch(e => console.error("Media-Reload 'reloadClick':", e));
	}

	//console.log('in apply3')
	window.addEventListener('click', reload, true);
	//console.log('in apply4')
}

apply()

